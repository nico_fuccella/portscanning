#!/usr/bin/python3

import socket

DEBUG = False

open_port_list = []

host = str(input("Enter the target IP: "))

begin_scan_range = 1
end_scan_range = 0

stop= True

def debug(str):
    if DEBUG == True:
        return print(str)

while stop==True:
    print("What type of scan do you want to do?\n")
    print("1) Fast Scan (ports 1-1024)")
    print("2) Slow Scan (ports 1-10000)")
    user_choice = int(input(">>> "))
    stop = False

    if user_choice == 1:
        end_scan_range = 1024
    elif user_choice == 2:
        end_scan_range = 10000
    else:
        stop = True

for port in range(begin_scan_range, end_scan_range):
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    if s.connect_ex((host, port)):
        debug ("Port %d is closed" % port)
    else:
        debug("Port %d is open" % port)
        open_port_list.append(port)

print ("Scan complete")
print (open_port_list)